Bug description:

There are two entities - Master and MyEntity
MyEntity is joined with Master via

@ManyToOne(fetch = FetchType.EAGER)
@JoinColumn(name = "master_id")
var masterEntity: MasterEntity = MasterEntity()

The problem is that @Query in MyEntityRepository

@Query("SELECT v FROM MyEntity v WHERE v.masterEntity = :masterEntity")
fun findByRelation(masterEntity: MasterEntity): List<MyEntity>

called from TestServiceImpl (Linked to @Get on 8080) fails on Named parameter not bound : masterEntity