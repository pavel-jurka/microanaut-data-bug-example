package com.example

import javax.inject.Singleton
import javax.transaction.Transactional

@Singleton
open class TestServiceImpl(
    private val repository: MyEntityRepository,
    private val masterRepository: MasterRepository
): TestService {

    @Transactional
    override fun init() {
        val masterEntity = masterRepository.findById(1L).get()
        repository.findByField(false)
        repository.findByRelation(masterEntity)
    }
}