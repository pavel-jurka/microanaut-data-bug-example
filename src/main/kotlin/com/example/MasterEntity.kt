package com.example

import javax.persistence.*

@Entity
@Table(name = MasterEntity.TABLE_NAME)
@SequenceGenerator(name = "master_entity_id_seq_gen", sequenceName = "master_entity_id_seq", allocationSize = 1)
class MasterEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "master_entity_id_seq_gen")
    var id: Long = 0L,

    ){

    companion object {
        const val TABLE_NAME = "master_entity"
    }
}