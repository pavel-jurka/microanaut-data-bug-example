package com.example

import io.micronaut.data.annotation.Query
import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.CrudRepository

@Repository
interface MyEntityRepository : CrudRepository<MyEntity, Long> {

    @Query("SELECT v FROM MyEntity v WHERE v.masterEntity = :masterEntity")
    fun findByRelation(masterEntity: MasterEntity): List<MyEntity>

    @Query("SELECT v FROM MyEntity v WHERE v.primary = :primary")
    fun findByField(primary: Boolean): List<MyEntity>
}
