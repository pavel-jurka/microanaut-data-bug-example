package com.example

import javax.persistence.*


@Entity
@Table(name = MyEntity.TABLE_NAME)
@SequenceGenerator(name = "my_entity_id_seq_gen", sequenceName = "my_entity_id_seq", allocationSize = 1)
class MyEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_entity_id_seq_gen")
    var id: Long = 0L,

    @Column(name ="primary_flag")
    var primary: Boolean = false,

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "master_id")
    var masterEntity: MasterEntity = MasterEntity()

) {

    companion object {
        const val TABLE_NAME = "my_entity"
    }
}
