package com.example

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller
class Controller(
    private val testService: TestService
) {

    @Get("/")
    fun test() {
        testService.init()
    }
}